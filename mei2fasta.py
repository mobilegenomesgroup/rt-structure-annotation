## DEPENDENCIES ##
# External
import os
import sys
import argparse

# Internal
import formats
import sequences
import retrotransposons

######################
## Get user's input ##
######################

## 1. Define parser ##
parser = argparse.ArgumentParser(description='')
parser.add_argument('vcf', help='')
parser.add_argument('outDir', help='Output directory')

## 2. Parse user input ##
args = parser.parse_args()
vcf = args.vcf
outDir = args.outDir

## 3. Display configuration to standard output ##
scriptName = os.path.basename(sys.argv[0])
scriptName = os.path.splitext(scriptName)[0]

print()
print('***** ', scriptName, 'configuration *****')
print('vcf: ', vcf)
print('outDir: ', outDir, "\n")


##########
## CORE ##
##########

## 1. Load vcf file
####################
VCF = formats.VCF()
VCF.read(vcf)

## 2. Create fasta object containing MEI sequences
########################################################

## Initialize fasta
fasta = formats.FASTA()

## Add solo L1 sequences to fasta
for variant in VCF.variants:

    ## Select L1s (solo + transductions) passing the filters
    if ('FAM' in variant.info) and ('STRAND' in variant.info) and (variant.info['VTYPE'] == 'somatic') and (variant.filter == 'PASS') and (variant.info['FAM'] == 'L1') and ((variant.info['ITYPE'] == 'solo') or (variant.info['ITYPE'] == 'partnered')):

        ## Add to the dict
        seqId = variant.chrom + ':' + str(variant.pos) + '_' + variant.info['STRAND'] + '_' + variant.ID
        fasta.seqDict[seqId] = variant.info['INSEQ']

    ## Select polyAs passing the filters
    elif (variant.info['VTYPE'] == 'somatic') and (variant.filter == 'PASS') and (variant.info['ITYPE'] == 'poly(A/T)'):

        ## Determine orientation (polyA:+, polyT:-)
        strand, polyA = retrotransposons.infer_strand_polyA_simple(variant.info['INSEQ'])

        ## Filter out if strand could not be determined (should be rare)
        if strand is None:
            continue

        ## Add to the dict
        seqId = variant.chrom + ':' + str(variant.pos) + '_' + strand + '_' + variant.ID
        fasta.seqDict[seqId] = variant.info['INSEQ']

## 3. Write fasta file as output
################################
outFile = outDir + '/solo_L1s.fa'
fasta.write(outFile)