#!/bin/bash


## Function 1. Print a section header for the string variable
function printHeader {
    string=$1
    echo "`date` ***** $string *****"
}

### Function 2. Execute and print to stdout commands 
function run {
    command=($1)
    if [[ $2 ]];then
         ${2}${command[@]}
    else
        echo -e "\n"" "${command[@]}
        eval ${command[@]}
    fi
}

##### 
## script aimed to...

# 1) 
# 2) 
# 3) 


## Get input parameters
vcf=$1
tdDb=$2
ref=$3
smallRna=$4
consensus=$5
outDir=$6
logDir=$7

echo
echo "******* Configuration *******"
echo "vcf: " $vcf
echo "tdDb: " $tdDb
echo "ref: " $ref
echo "smallRna: " $smallRna
echo "consensus: " $consensus
echo "outDir: " $outDir
echo "logDir: " $logDir
echo "*****************************"
echo

## Activate python environment
source activate py36

## Scripts
mei2fasta=/Users/brodriguez/Research/Projects/MEIGA/Scripts/python/mei2fasta.py
trimTsd=/Users/brodriguez/Research/Projects/MEIGA/Scripts/python/trim_tsd.v2.py
trimPolyA=/Users/brodriguez/Research/Projects/MEIGA/Scripts/python/trim_polyA.py
revMinus=/Users/brodriguez/Research/Projects/HGSVC2/Scripts/python/reverse2forward.py
structure=/Users/brodriguez/Research/Projects/MEIGA/Scripts/python/annotate_L1s.v6.py

### 1) Collect mei sequences
#################################
printHeader "1) Collect solo L1 sequences"
run "python $mei2fasta $vcf $outDir 1> $logDir/collect_solo_L1.out 2> $logDir/collect_solo_L1.err" "$ECHO" 
echo


### 2) Annotate and trim target site duplications
##################################################
input=$outDir/solo_L1s.fa
outName='solo_L1s.tsd.fa'

printHeader "2) Annotate and trim target site duplications"
run "python $trimTsd $input $ref $outName $outDir 1> $logDir/annotate_trim_tsd.out 2> $logDir/annotate_trim_tsd.err" "$ECHO" 
echo


### 3) Trim PolyA/T tails
##########################
## Note: Generate file with polyA/T sequence annotation. Useful for some analyses
#input=$outDir/solo_L1s.tsd.fa
#outName='solo_L1s.tsd.polyA.fa'

#printHeader "3) Trim PolyA/T tails"
#run "python $trimPolyA $input $outName $outDir 1> $logDir/trim_polyA.out 2> $logDir/trim_polyA.err" "$ECHO" 
#echo


### 4) Reverse sequences in - orientation
##########################################
## Note: Generate file with ids of sequences reversed
input=$outDir/solo_L1s.tsd.fa
outName='solo_L1s.tsd.rev.fa'

printHeader "4) Reverse sequences in - orientation"
run "python $revMinus $input $outName $outDir 1> $logDir/rev_minus.out 2> $logDir/rev_minus.err" "$ECHO" 
echo


### 5) Annotate structure and microhomologies for L1 inserts
#############################################################
## Note: Generate file with ids of sequences reversed
input=$outDir/solo_L1s.tsd.rev.fa
outName='structure_annotation'

printHeader "5) Annotate structure and microhomologies for L1 inserts"
run "python $structure $input $tdDb $consensus $ref $smallRna $outName $outDir 1> $logDir/structure_annot.out 2> $logDir/structure_annot.err" "$ECHO" 
echo

printHeader "DONE"
echo