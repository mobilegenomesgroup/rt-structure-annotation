## DEPENDENCIES ##
# External
import os
import sys
import argparse

# Internal
import formats
import sequences
import retrotransposons

######################
## Get user's input ##
######################

## 1. Define parser ##
parser = argparse.ArgumentParser(description='')
parser.add_argument('fasta', help='')
parser.add_argument('outName', help='')
parser.add_argument('outDir', help='Output directory')

## 2. Parse user input ##
args = parser.parse_args()
fasta = args.fasta
outName = args.outName
outDir = args.outDir

## 3. Display configuration to standard output ##
scriptName = os.path.basename(sys.argv[0])
scriptName = os.path.splitext(scriptName)[0]

print()
print('***** ', scriptName, 'configuration *****')
print('fasta: ', fasta)
print('outName: ', outName)
print('outDir: ', outDir, "\n")


##########
## CORE ##
##########

## 1. Load fasta file
####################
FASTA = formats.FASTA                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               ()
FASTA.read(fasta)

## 2. Create fasta object
#########################

## Parse inserts, one at a time
for seqId, seq in FASTA.seqDict.items():

    strand = seqId.split('_')[1]

    ## Reverse sequence for insertions with - orientation
    if strand == '-':
        print('REV')
        seq = sequences.rev_complement(seq)   

    ## Add to the dict
    FASTA.seqDict[seqId] = seq


## 3. Write fasta file as output
################################
outFile = outDir + '/' + outName
FASTA.write(outFile)