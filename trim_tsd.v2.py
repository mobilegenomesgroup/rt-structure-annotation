## DEPENDENCIES ##
# External
import os
import sys
import argparse
import pandas as pd
import pysam

# Internal
import formats
import sequences
import alignment

#### FUNCTION ####

def search4tsd(targetSeq, insert):
    '''
    '''
    tsdLen = 0

    for i in range(0, len(targetSeq)):
        if targetSeq[i] != insert[i]:
            break

        tsdLen += 1
    
    tsdSeq  = insert[0:tsdLen]
    insert = insert[tsdLen:]

    return tsdSeq, tsdLen, insert


######################
## Get user's input ##
######################

## 1. Define parser ##
parser = argparse.ArgumentParser(description='')
parser.add_argument('inserts', help='')
parser.add_argument('genome', help='Reference genome')
parser.add_argument('outName', help='Outname')
parser.add_argument('outDir', help='Output directory')

## 2. Parse user input ##
args = parser.parse_args()
inserts = args.inserts
genome = args.genome
outName = args.outName
outDir = args.outDir

## 3. Display configuration to standard output ##
scriptName = os.path.basename(sys.argv[0])
scriptName = os.path.splitext(scriptName)[0]

print()
print('***** ', scriptName, 'configuration *****')
print('inserts: ', inserts)
print('genome: ', genome)
print('outName: ', outName)
print('outDir: ', outDir, "\n")


##########
## CORE ##
##########

## Read input sequences
#########################
### Inserts
inserts_fasta = formats.FASTA()
inserts_fasta.read(inserts)

## Search and trim tsd 
#######################
genome = pysam.FastaFile(genome)

trimmed_fasta = formats.FASTA()

tsd_annot = [['insId', 'coordId', 'tsdLen', 'tsdSeq']]

for id, seq in inserts_fasta.seqDict.items():
    
    if ('random' in id) or ('Un' in id):
        continue
    
    ref, bkp = id.split('_')[0].split(':')
    insId = id.split('_')[2]
    coordId = id.split('_')[0] + '_' + id.split('_')[1]

    targetCoord = ref + ':' + bkp + '-' + str(int(bkp) + 30)
    targetSeq = genome.fetch(region=targetCoord)

    ## Search for the TSD in 3 offsets
    tsdSeq1, tsdLen1, trimmed1 = search4tsd(targetSeq, seq)
    tsdSeq2, tsdLen2, trimmed2 = search4tsd(targetSeq[1:], seq)
    tsdSeq3, tsdLen3, trimmed3 = search4tsd(targetSeq[2:], seq)

    ## Select offset for largest TSD
    tmp = {tsdLen1:'tsdLen1', tsdLen2:'tsdLen2', tsdLen3:'tsdLen3'}
    tsd_offset = tmp.get(max(tmp))

    if tsd_offset == 'tsdLen1':
        tsdSeq, tsdLen, trimmed = (tsdSeq1, tsdLen1, trimmed1)

    elif tsd_offset == 'tsdLen2':
        tsdSeq, tsdLen, trimmed = (tsdSeq2, tsdLen2, trimmed2)

    else:
        tsdSeq, tsdLen, trimmed = (tsdSeq3, tsdLen3, trimmed3)

    if tsdLen == 0:
        tsdSeq = '-'

    trimmed_fasta.seqDict[id] = trimmed

    ## Add TSD annot
    tsd_annot.append([insId, coordId, tsdLen, tsdSeq])

## Write to outfile
outFile = outDir + '/' + outName
trimmed_fasta.write(outFile)

## TO ADD. Write table with TSD len and sequence per loci
df = pd.DataFrame(tsd_annot[1:],columns=tsd_annot[0])

outFile = outDir + '/TSD_annot.tsv'
df.to_csv(outFile, index=False, sep ='\t')
