## DEPENDENCIES ##
# External
import os
import sys
import argparse
import pandas as pd

# Internal
import formats
import retrotransposons

######################
## Get user's input ##
######################

## 1. Define parser ##
parser = argparse.ArgumentParser(description='')
parser.add_argument('fasta', help='')
parser.add_argument('outName', help='Outname')
parser.add_argument('outDir', help='Output directory')

## 2. Parse user input ##
args = parser.parse_args()
fasta = args.fasta
outName = args.outName
outDir = args.outDir

## 3. Display configuration to standard output ##
scriptName = os.path.basename(sys.argv[0])
scriptName = os.path.splitext(scriptName)[0]

print()
print('***** ', scriptName, 'configuration *****')
print('fasta: ', fasta)
print('outName: ', outName)
print('outDir: ', outDir, "\n")


##########
## CORE ##
##########

## 1. Load fasta file
####################
FASTA = formats.FASTA                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               ()
FASTA.read(fasta)

## 2. Collect polyA/T tails
############################
## Initiate dataframe
annotDf = pd.DataFrame(columns=['polyLen', 'polySeq'])

## Parse inserts, one at a time
for seqId, seq in FASTA.seqDict.items():

    strand = seqId.split('_')[1]

    if strand == '+':
        ## Trim PolyA sequence
        trimmed, polyLen, polySeq = retrotransposons.trim_polyA(seq)

    else:
        ## Trim PolyT sequence
        trimmed, polyLen, polySeq = retrotransposons.trim_polyT(seq)        
    
    ## Add polyA/T seq to the fasta
    FASTA.seqDict[seqId] = trimmed

    ## Add polyA/T annotation to the dataframe
    annotDf.loc[seqId] = [polyLen, polySeq]

## 3. Write fasta file as output
################################
outFile = outDir + '/' + outName
FASTA.write(outFile)


## 4. Write dataframe as output
################################
## Write dataframe
outFile = outDir + '/polyA_annot.tsv'
annotDf.to_csv(outFile, sep='\t', index=True)
