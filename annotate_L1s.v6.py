## DEPENDENCIES ##
# External
import os
import sys
import argparse
import pandas as pd
import subprocess
from operator import attrgetter

# Internal
import formats
import sequences
import alignment
import unix
import log
import structures


###############
## Functions ##
###############

def fetch_ref_seq(ref, coord, fileName, outDir):
    '''
    Wrapper to convert a SAM into a PAF file
    '''
    FASTA = outDir + '/' + fileName + '.fa'
    err = open(outDir + '/fetch.err', 'w') 

    command = 'samtools faidx ' + ref + ' ' + coord + ' > ' + FASTA
    subprocess.call(command, stderr=err, shell=True)

    return FASTA

def search4secondMolecule(seq):
    '''
    '''
    targetMonomer = 'T'
    windowSize = 20
    maxWindowDist = 1
    minMonomerSize = 20
    minPurity = 90

    monomers = sequences.find_monomers(seq, targetMonomer, windowSize, maxWindowDist, minMonomerSize, minPurity)

    if monomers:
        polyT = seq[:monomers[-1].end]
        twoMolBridge = True

    else:
        polyT = None
        twoMolBridge = False

    return twoMolBridge, polyT

def search4transductions(seq):
    '''
    '''
    targetMonomer = 'A'
    windowSize = 10
    maxWindowDist = 1
    minMonomerSize = 15
    minPurity = 90

    monomers = sequences.find_monomers(seq, targetMonomer, windowSize, maxWindowDist, minMonomerSize, minPurity)

    ## A) No monomers. Solo without polyA 
    if len(monomers) == 0:
        iType = 'solo'
        nbTd = 0
        tsdLen = 'NA'
        sourceId = 'NA'
        tdRegionCoord = 'NA'
        tdMAPQ = 'NA'
        tdSeq = 'NA'
        polyA = None

        ## Trimming
        trimmed = seq

    ## B) One monomer. Solo ins with polyA
    elif len(monomers) == 1:
        nbTd = 0
        tsdLen = 'NA'
        sourceId = 'NA'
        tdRegionCoord = 'NA'
        tdMAPQ = 'NA'
        tdSeq = 'NA'
        polyA = seq[monomers[0].beg:]

        ## PolyA trimming
        trimmed = seq[:monomers[0].beg]

        ## a) No sequence left after trimming -> poly(A/T) 
        if len(trimmed) == 0:
            iType = 'poly(A/T)'
            trimmed = 'NA'

        ## b) Sequence left post-trimming -> potential solo/transduction/etc
        else:
            iType = 'solo'

    ## C) Two monomers. Partnered transduction
    elif len(monomers) == 2:
        iType = 'partnered'
        nbTd = len(monomers) - 1

        ## PolyAs plus transduction trimming
        trimmed = seq[:monomers[0].beg]

        ## Collection of transduced sequence
        tdSeq = seq[monomers[0].end:monomers[-1].beg]   
        polyA = seq[monomers[0].beg:]

        ## Create fasta containing TSD seq
        fasta = formats.FASTA()
        fasta.seqDict[seqId] = tdSeq
        FASTA_tsd = outDir + '/TSD_' + seqId + '.fa'
        fasta.write(FASTA_tsd)

        ## Align the tsd insert into the retrotransposon sequences database
        SAM_file = alignment_bwa(FASTA_tsd, tdDb, 'tsd_alignment_' + seqId, 1, outDir)     
        PAF_file = alignment.sam2paf(SAM_file, 'tsd_alignment_' + seqId, outDir)

        ## Read alignments
        PAF = formats.PAF()
        PAF.read(PAF_file)

        ## Cleanup
        unix.rm([SAM_file, PAF_file, FASTA_tsd])

        ## Filter alignments
        minPerc = 80
        nbUnfiltered = len(PAF.alignments)
        PAF.alignments = [hit for hit in PAF.alignments if hit.alignmentPerc() > minPerc]
        nbHits = len(PAF.alignments)

        ## Make decision
        # Criteria for doing assignation:
        #       - Single hit after filtering
        #       - Minimum MAPQ of 1. NOTE: do not apply it for now and include the MAPQ together with the assignation for now
        # a) Known source
        if nbHits == 1:
            hit = PAF.alignments[0]
            tsdLen = hit.qLen
            sourceId = hit.tName
            tdRegionCoord = str(hit.tBeg) + ':' + str(hit.tEnd)
            tdMAPQ = hit.MAPQ 

        # b) Unknown source
        else:
            tsdLen = len(tdSeq)
            sourceId = 'UNK'
            tdRegionCoord = 'NA'
            tdMAPQ = 'NA'

    ## D) Three monomers. 2 Nested transductions
    elif len(monomers) == 3:
        iType = 'partnered'
        nbTd = len(monomers) - 1

        ## PolyAs plus transductions trimming
        trimmed = seq[:monomers[0].beg]
        polyA = seq[monomers[0].beg:]

        ## Collection of first transduced sequence
        tdSeq = seq[monomers[0].end:monomers[1].beg]

        ## Create fasta containing TSD seq
        fasta = formats.FASTA()
        fasta.seqDict[seqId] = tdSeq
        FASTA_tsd = outDir + '/TSD_' + seqId + '.fa'
        fasta.write(FASTA_tsd)

        ## Align the tsd insert into the retrotransposon sequences database
        SAM_file = alignment_bwa(FASTA_tsd, tdDb, 'tsd_alignment_' + seqId, 1, outDir)     
        PAF_file = alignment.sam2paf(SAM_file, 'tsd_alignment_' + seqId, outDir)

        ## Read alignments
        PAF = formats.PAF()
        PAF.read(PAF_file)

        ## Cleanup
        unix.rm([SAM_file, PAF_file, FASTA_tsd])

        ## Filter alignments
        minPerc = 80
        nbUnfiltered = len(PAF.alignments)
        PAF.alignments = [hit for hit in PAF.alignments if hit.alignmentPerc() > minPerc]
        nbHits = len(PAF.alignments)

        ## Make decision
        # Criteria for doing assignation:
        #       - Single hit after filtering
        #       - Minimum MAPQ of 1. NOTE: do not apply it for now and include the MAPQ together with the assignation for now
        # a) Known source
        if nbHits == 1:
            hit = PAF.alignments[0]
            tsdLen = hit.qLen
            sourceId = hit.tName
            tdRegionCoord = str(hit.tBeg) + ':' + str(hit.tEnd)
            tdMAPQ = hit.MAPQ 

        # b) Unknown source, multiple hits
        elif nbHits > 1:
            tsdLen = hit.qLen
            sourceId = 'UNK'
            tdRegionCoord = 'NA'
            tdMAPQ = 'NA'

        # c) Unknown source, no hits
        else:
            tsdLen = len(tdSeq)
            sourceId = 'UNK'
            tdRegionCoord = 'NA'
            tdMAPQ = 'NA'

    ## E) More than two nested events. Very rare cases, not resolve from now. In the future we could map back the sequence of jumps and source L1
    else:
        iType = 'partnered'
        nbTd = len(monomers) - 1
        tsdLen = 'NA'
        sourceId = 'NA'
        tdRegionCoord = 'NA'
        tdMAPQ = 'NA'

        ## Collection of first transduced sequence
        tdSeq = seq[monomers[0].end:monomers[1].beg]
        
        ## PolyAs plus transduction trimming
        trimmed = seq[:monomers[0].beg]
        polyA = seq[monomers[0].beg:]

    return iType, nbTd, tsdLen, sourceId, tdRegionCoord, tdMAPQ, tdSeq, trimmed, polyA
    

def alignment_bwa(FASTA, reference, fileName, processes, outDir):
    '''
    Align a set of sequence into a reference with bwa mem

    Input:
        1. FASTA: Path to FASTA file with sequences to align
        2. reference: Path to the reference genome in fasta format (bwa mem index must be located in the same folder)
        3. fileName: output file will be named accordingly
        4. processes: Number of processes used by bwa mem
        5. outDir: Output directory

    Output:
        1. SAM: Path to SAM file containing input sequences alignments or 'None' if alignment failed 
    '''
    ## Align the sequences into the reference
    SAM = outDir + '/' + fileName + '.sam'
    err = open(outDir + '/align.err', 'w') 
    command = 'bwa mem -k 8 -T 0 -Y -t ' + str(processes) + ' ' + reference + ' ' + FASTA + ' > ' + SAM
    status = subprocess.call(command, stderr=err, shell=True)

    if status != 0:
        step = 'ALIGN'
        msg = 'Alignment failed' 
        log.step(step, msg)

    return SAM

def target_realignment(seqId, seq, chain, consensus):
    '''
    '''    
    ## Create fasta with sequence to realign
    seq2realign = formats.FASTA()
    seq2realign.seqDict[seqId] = seq

    FASTA_file = outDir + '/seq2realign_' + seqId + '.fa'
    seq2realign.write(FASTA_file)

    ## Realign
    SAM_file = alignment_bwa(FASTA_file, consensus, 'realignment_' + seqId, 1, outDir)
        
    ## SAM to PAF conversion
    PAF_file = alignment.sam2paf(SAM_file, 'realignment_' + seqId, outDir)
    PAF = formats.PAF()
    PAF.read(PAF_file)

    ## Merge alignments 
    PAF.alignments = PAF.alignments + chain.alignments

    ## Redo the chain
    maxDist = 10
    maxPercOverlap = 20 
    new_chain = PAF.chain(maxDist, maxPercOverlap)

    ## Evaluate the chain -> discard in case of uncertainty 
    if len(new_chain.alignments) > 2: 
        final_chain = chain

    elif (len(new_chain.alignments) == 2) and ((new_chain.alignments[0].strand != '-') or (new_chain.alignments[1].strand != '+')):
        final_chain = chain

    else:
        final_chain = new_chain

    ## Cleanup
    unix.rm([FASTA_file, SAM_file, PAF_file, outDir + '/align.err', outDir + '/sam2paf.err'])

    return final_chain


def trim_3prime_td(insId, annotDict, chain):
    '''
    '''
    print('TRIM_3_PRIME: ', insId, annotDict, chain.alignments[-1])

    ## 1. Check for and trim 5' transductions

    ## 2. Check for and trim 3' transductions

    ## 3. Annotate 

def insertion_structure(insId, annot, chain):
    '''
    '''
    rtLen = len(annot['trimmed'])
    nbAlignments = len(chain.alignments)

    ## A) Single alignment in forward -> FULL or TRUNCATION (TRUN)
    if (nbAlignments == 1) and (chain.alignments[0].strand == '+'):

        percLen = (chain.alignments[0].qLen / 6000) * 100

        # a) FULL
        if percLen > 95:
            annot['status'] = 'FULL'
            annot['trunLen'] = 'NA'        

        # b) DEL
        else:
            annot['status'] = 'TRUN'
            annot['trunLen'] = str(chain.alignments[0].tBeg)

        ## Define alignment coordinates
        annot['noInvCoordQ'] = str(chain.alignments[0].qBeg) + ':' + str(chain.alignments[0].qEnd)
        annot['noInvCoordT'] = str(chain.alignments[0].tBeg) + ':' + str(chain.alignments[0].tEnd)  
    
    ## B) Single alignment in reverse -> INV without resolved 3' end 
    elif (nbAlignments == 1) and (chain.alignments[0].strand == '-'):

        annot['status'] = 'INV'

        ## Define alignment coordinates
        annot['invLen'] = chain.alignments[0].qEnd - chain.alignments[0].qBeg
        annot['invCoordQ'] = str(chain.alignments[0].qBeg) + ':' + str(chain.alignments[0].qEnd)
        annot['noInvCoordQ'] = str(chain.alignments[0].qEnd) + ':' + str(rtLen)

        annot['invCoordT'] = str(chain.alignments[0].tBeg) + ':' + str(chain.alignments[0].tEnd)
        annot['noInvCoordT'] = str(chain.alignments[0].tEnd) + ':' + '6023'
    
    ## C) Two alignment (-,+) -> completely resolved INV
    elif (nbAlignments == 2) and (chain.alignments[0].strand == '-') and (chain.alignments[1].strand == '+'):
       
        annot['status'] = 'INV'
        
        ## Define alignment coordinates
        annot['invLen'] = chain.alignments[0].qEnd - chain.alignments[0].qBeg
        annot['invCoordQ'] = str(chain.alignments[0].qBeg) + ':' + str(chain.alignments[0].qEnd)
        annot['noInvCoordQ'] = str(chain.alignments[1].qBeg) + ':' + str(chain.alignments[1].qEnd)

        annot['invCoordT'] = str(chain.alignments[0].tBeg) + ':' + str(chain.alignments[0].tEnd)
        annot['noInvCoordT'] = str(chain.alignments[1].tBeg) + ':' + str(chain.alignments[1].tEnd)   

        ## Check if del/dup at inversion junction
        junc_status, annot['junc_del_len'], annot['junc_dup_len'] = junction_status(chain)

        ## Redefine status incorporating inversion junction status
        if junc_status in ['IDEL', 'IDUP']:
            annot['status'] = 'INV' + '+' + junc_status

    ## D) Two alignment (+,+) -> insertion with interstitial deletion
    elif (nbAlignments == 2) and (chain.alignments[0].strand == '+') and (chain.alignments[1].strand == '+'):
        annot['status'] = 'IDEL' 

    ## E) Complex
    else: 
        nbHits = len(chain.alignments)
        annot['status'] = 'COMPLEX(' + str(nbHits) + ')'

    return annot

def junction_status(chain):
    '''
    '''
    distQ = chain.alignments[0].qEnd - chain.alignments[1].qBeg
    distT = chain.alignments[0].tEnd - chain.alignments[1].tBeg 
    distT_adj = distT - distQ # Adjust distance for template

    diffT = abs(distT_adj - distT) # Difference between adjusted and original template distance
    diffT_norm =  (diffT + 0.00001) / (abs(distT) + 0.00001) * 100 # normalize difference by original template dist.  

    ## Insterstitial deletion
    if (distT_adj < 0) and (diffT_norm <= 50):
        status = 'IDEL' 
        junc_del_len = distT_adj * -1
        junc_dup_len = 'NA'

    ## Interstitial duplication
    elif (distT_adj > 0) and (diffT_norm <= 50):
        status = 'IDUP' 
        junc_del_len = 'NA'
        junc_dup_len = distT_adj

    ## Blunt joint
    elif distT_adj in [-1, 0, 1]:
        status = 'BLUNT' 
        junc_del_len = 'NA'
        junc_dup_len = 'NA'

    else:
        status = 'UNK' 
        junc_del_len = 'NA'
        junc_dup_len = 'NA'

    return status, junc_del_len, junc_dup_len


def alignment_polishing(seqId, chain, trimmed, consensus):
    '''
    '''
    # a) Attempt to recover undetected 5' inversion
    if (len(chain.alignments) == 1) and (chain.alignments[0].strand == '+'):

        ## Define sequence to realign
        qBeg = chain.interval()[0]
        seq2realign = trimmed[:qBeg]  

        ## Realignment
        chain = target_realignment(seqId, seq2realign, chain, consensus)
    
    # b) Attempt to recover undetected 3' end
    elif (len(chain.alignments) == 1) and (chain.alignments[0].strand == '-'):

        ## Define sequence to realign
        qEnd = chain.interval()[1]
        seq2realign = seq[qEnd:]  
        
        ## Realignment
        chain = target_realignment(seqId, seq2realign, chain, consensus)

    return chain

def align2consensus(seqId, trimmed, consensus, outDir):
    '''
    '''
    ## Create fasta with L1 insert
    insert = formats.FASTA()
    insert.seqDict[seqId] = trimmed
    FASTA_insert = outDir + '/L1_insert_' + seqId + '.fa'
    insert.write(FASTA_insert)

    ## Align the insert into the consensus L1 sequence
    SAM_file = alignment_bwa(FASTA_insert, consensus, 'alignment_' + seqId, 1, outDir)     
    PAF_file = alignment.sam2paf(SAM_file, 'alignment_' + seqId, outDir)

    ## Read PAF alignments
    PAF = formats.PAF()
    PAF.read(PAF_file)

    ## Cleanup
    unix.rm([FASTA_insert, SAM_file, PAF_file, outDir + '/align.err', outDir + '/sam2paf.err'])

    ## Return
    return PAF


def align2ref(seqId, trimmed, reference, outDir):
    '''
    '''
    ## Create fasta with L1 insert
    insert = formats.FASTA()
    insert.seqDict[seqId] = trimmed
    FASTA_insert = outDir + '/L1_insert_' + seqId + '.fa'
    insert.write(FASTA_insert)

    ## Align the insert into the reference genome
    SAM_file = alignment_bwa(FASTA_insert, reference, 'alignment_ref_' + seqId, 1, outDir)     
    PAF_file = alignment.sam2paf(SAM_file, 'alignment_ref_' + seqId, outDir)

    ## Read PAF alignments
    PAF = formats.PAF()
    PAF.read(PAF_file)

    ## Cleanup
    unix.rm([FASTA_insert, SAM_file, PAF_file, outDir + '/align.err', outDir + '/sam2paf.err'])

    ## Return
    return PAF

def align2ref_target(seqId, trimmed, reference, outDir):
    '''
    '''
    ## Collect reference sequence at the integration point
    offset = 10000
    insLen = len(trimmed)
    ref = seqId.split('_')[0].split(':')[0]
    beg = str(int(seqId.split('_')[0].split(':')[1]) - insLen - offset)
    end = str(int(seqId.split('_')[0].split(':')[1]) + insLen + offset)
    interval = ref + ':' + beg + '-' + end
    fileName = 'ref_' + seqId
    FASTA_ref = fetch_ref_seq(reference, interval, fileName, outDir)

    ## Generate fasta file containing inserted sequence
    insert = formats.FASTA()
    insert.seqDict[seqId] = trimmed
    FASTA_insert = outDir + '/insert_' + seqId + '.fa'
    insert.write(FASTA_insert)
    fileName = 'alignment2ref_' + seqId

    ## Align inserted sequence against reference sequence
    PAF_file = alignment.alignment_minimap2(FASTA_insert, FASTA_ref, fileName, 1, outDir)

    ## Read alignments file
    PAF = formats.PAF()
    PAF.read(PAF_file)

    ## Cleanup
    unix.rm([FASTA_ref, FASTA_insert, PAF_file, outDir + '/align.err'])

    return PAF

def tandem_dup(seq, seqId, reference, outDir):
    '''
    '''
    ## Collect reference sequence at the integration point
    offset = 100
    insLen = len(seq)
    ref = seqId.split('_')[0].split(':')[0]
    beg = str(int(seqId.split('_')[0].split(':')[1]) - insLen - offset)
    end = str(int(seqId.split('_')[0].split(':')[1]) + insLen + offset)
    interval = ref + ':' + beg + '-' + end
    fileName = 'ref_' + seqId
    FASTA_ref = fetch_ref_seq(reference, interval, fileName, outDir)

    ## Generate fasta file containing inserted sequence
    insert = formats.FASTA()
    insert.seqDict[seqId] = seq
    FASTA_insert = outDir + '/insert_' + seqId + '.fa'
    insert.write(FASTA_insert)
    fileName = 'alignment2ref_' + seqId

    ## Align inserted sequence against reference sequence
    PAF_file = alignment.alignment_minimap2(FASTA_insert, FASTA_ref, fileName, 1, outDir)

    ## Read alignments file
    PAF = formats.PAF()
    PAF.read(PAF_file)

    ## Filter out partial alignments
    minPerc = 95
    PAF.alignments = [hit for hit in PAF.alignments if hit.alignmentPerc() > minPerc]
            
    ## Decide if tandem dup or not
    nbAlignments = len(PAF.alignments)

    # a) Tandem dup. At least one full alignment of the inserted sequence at the insertion region
    if (nbAlignments > 0):
        is_dup = True

    # b) Not tandem dup. No full alignments
    else:
        is_dup = False
    
    ## Cleanup
    unix.rm([FASTA_ref, FASTA_insert, PAF_file, outDir + '/align.err'])

    return is_dup

def hits2binDb(hits):
    '''
    '''
    ## Organize hits into a dictionary 
    hitsDict = {}
    hitsDict['CONSENSUS'] = []

    for hit in hits:
        hit.beg = hit.qBeg
        hit.end = hit.qEnd
        hitsDict['CONSENSUS'].append(hit)


    ## Create bin database
    iLen = len(annotDict[insId]['trimmed'])

    binSizes = [100, 1000, 10000]
    binDb = structures.create_bin_database_interval('insert', 0, iLen, hitsDict, binSizes)

    return binDb

def filter_hits(hits, refHitsBinDb, maxOverlap):
    '''
    '''
    filteredHits = []

    for hit in hits:

        overlaps = refHitsBinDb.collect_interval(hit.qBeg, hit.qEnd, ['CONSENSUS'])

        if not overlaps:
            filteredHits.append(hit)
            continue

        overlaps.sort(key=lambda x: x[2], reverse=True)
        percOverlap = overlaps[0][2]

        if percOverlap < maxOverlap:
            filteredHits.append(hit)
            continue            
    
    return filteredHits


######################
## Get user's input ##
######################

## 1. Define parser ##
parser = argparse.ArgumentParser(description='')
parser.add_argument('L1s', help='fasta containing input L1 sequences')
parser.add_argument('tdDb', help='fasta containing transduced sequences')
parser.add_argument('consensus', help='consensus L1 sequence in fasta format')
parser.add_argument('reference', help='reference genome sequence')
parser.add_argument('smallRNAs', help='small rnas')
parser.add_argument('outName', help='Output file name')
parser.add_argument('outDir', help='Output directory')

## 2. Parse user input ##
args = parser.parse_args()
L1s = args.L1s
tdDb = args.tdDb
consensus = args.consensus
reference = args.reference
smallRNAs = args.smallRNAs
outName = args.outName
outDir = args.outDir

## 3. Display configuration to standard output ##
scriptName = os.path.basename(sys.argv[0])
scriptName = os.path.splitext(scriptName)[0]

print()
print('***** ', scriptName, 'configuration *****')
print('L1s: ', L1s)
print('tdDb: ', tdDb)
print('consensus: ', consensus)
print('reference: ', reference)
print('smallRNAs: ', smallRNAs)
print('outName: ', outName)
print('outDir: ', outDir, "\n")


##########
## CORE ##
##########

## 1. Read input L1 sequences
###########################
inputFasta = formats.FASTA()
inputFasta.read(L1s)

## 2. Search for 3' transductions
##################################
annotDict = {}

## For each input L1 sequence
for seqId, seq in inputFasta.seqDict.items():

    ## Select only solo poly(A/T) insertions
    #if seqId not in ['chr1:205929814_-_ID1', 'chr11:107053212_-_ID23', 'chr12:19974550_-_ID24', 'chr15:24182884_-_ID25', 'chr15:97232367_+_ID242', 'chr16:25944960_-_ID27', 'chr16:52996096_+_ID28', 'chr17:77402566_+_ID30', 'chr18:53187686_+_ID31', 'chr19:464342_-_ID32', 'chr19:22950682_+_ID33', 'chr2:136711417_+_ID2', 'chr2:180353318_+_ID4', 'chr3:80944847_-_ID67', 'chr3:86868689_+_ID5', 'chr3:109107834_-_ID6', 'chr3:157458298_-_ID8', 'chr3:194849431_+_ID9', 'chr5:143262011_-_ID10', 'chr5:173400260_-_ID11', 'chr6:34800169_+_ID12', 'chr6:52273000_+_ID13', 'chr6:62334398_-_ID14', 'chr7:27584083_+_ID16', 'chr7:109401332_+_ID18', 'chr8:91385884_-_ID19', 'chr8:111983780_-_ID178', 'chr8:112228441_-_ID20', 'chr9:119391770_-_ID21', 'chrX:13710247_-_ID34', 'chrX:63309134_+_ID292']:
    #    continue

    ## Select only insertions with miscalled transduced sequences
    #if seqId not in ['chr15:31943182_-_ID1099', 'chr15:37602309_+_ID1104', 'chr15:86728646_-_ID1113', 'chr15:87117073_+_ID1114', 'chr17:47474054_+_ID1160', 'chr19:24374170_-_ID1206', 'chr1:81410701_+_ID45', 'chr2:29390561_+_ID127', 'chr6:123065906_+_ID534', 'chr6:135690898_-_ID540', 'chr6:68928159_-_ID502', 'chr6:92110780_+_ID514', 'chr6:95506731_+_ID517', 'chr7:126059160_-_ID640', 'chrX:66810595_-_ID1282']:
    #    continue     
    
    ## Select only insertions containing templated nucleotides
    #if seqId not in ['chr1:78874247_-_ID42', 'chr12:20526388_-_ID952', 'chr14:62885053_+_ID1059', 'chr16:6387648_-_ID1121', 'chr16:52250893_-_ID1142', 'chr18:64586964_-_ID1193', 'chr19:30826944_-_ID1215', 'chr19:31991512_-_ID1219', 'chr2:118480675_-_ID190', 'chr3:115305027_-_ID282', 'chr3:148297312_-_ID309', 'chr4:18118891_-_ID333', 'chr5:20332091_-_ID422', 'chr5:31252156_-_ID431', 'chr6:145619869_+_ID544', 'chr7:40510110_+_ID575', 'chr7:51545090_+_ID577', 'chr7:97779469_+_ID619', 'chrX:78840331_-_ID1287', 'chrX:85482808_-_ID1292']:
    #    continue

    ## Ids
    insId = seqId.split('_')[2]
    coordId = seqId.split('_')[0] + '_' + seqId.split('_')[1]
    
    ## 3' transduction search
    iType, nbTd, tsdLen, sourceId, tdRegionCoord, tdMAPQ, tdSeq, trimmed, polyA = search4transductions(seq)
    
    ## Add transduction annotation and trimmed insert without transduction and polyA tails to the dict
    annotDict[insId] = {}
    annotDict[insId]['coordId'] = coordId
    annotDict[insId]['iType'] = iType
    annotDict[insId]['iLen'] = len(seq)
    annotDict[insId]['nbTd'] = nbTd
    annotDict[insId]['tsdLen'] = tsdLen
    annotDict[insId]['sourceId'] = sourceId
    annotDict[insId]['tdRegionCoord'] = tdRegionCoord
    annotDict[insId]['tdMAPQ'] = tdMAPQ
    annotDict[insId]['tdSeq'] = tdSeq
    annotDict[insId]['trimmed'] = trimmed
    annotDict[insId]['polyA'] = polyA
    annotDict[insId]['hits_consensus'] = formats.PAF()
    annotDict[insId]['hits_td_db'] = formats.PAF()
    annotDict[insId]['hits_ref'] = formats.PAF()
    annotDict[insId]['hits_snRNA'] = formats.PAF()
    annotDict[insId]['seq'] = seq
    annotDict[insId]['templated'] = 'None'
    annotDict[insId]['templated_coord'] = 'NA'
    annotDict[insId]['templated_dist'] = 'NA'
    annotDict[insId]['templated_len'] = 'NA'

    ## Second molecule search == polyT on the 5'
    twoMol, polyT = search4secondMolecule(trimmed)
    annotDict[insId]['polyT'] = polyT
    annotDict[insId]['twoMol'] = twoMol

## 3. Align trimmed inserts (without polyA/T tails, transduced sequence/s and TSD) against multiple references
###############################################################################################################

#### 3.1 Generate fasta with all the trimmed inserts
inserts = formats.FASTA()

for insId in annotDict.keys():

    ## Skip if already annotated as poly(A/T) and therefore there is not trimmed sequence left to align
    if annotDict[insId]['iType'] == 'poly(A/T)':
        #print('POLY')

        #TODO:
        # annotation = [insId, coordId, iType, nbTd, tsdLen, sourceId, tdRegionCoord, tdMAPQ, 'TEMPLATED', '0', len(trimmed), 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', tdSeq]
        #outList.append(annotation)        
        continue

    ## Add to trimmed seq to the fasta dict
    inserts.seqDict[insId] = annotDict[insId]['trimmed']

FASTA_insert = outDir + '/trimmed_L1_inserts.fa'
inserts.write(FASTA_insert)

#### 3.2 Align against consensus L1 sequence ####
SAM_consensus = alignment_bwa(FASTA_insert, consensus, 'alignment2consensus', 2, outDir)     
PAF_consensus = alignment.sam2paf(SAM_consensus, 'alignment2consensus', outDir)
#PAF_consensus = "/Users/brodriguez/Research/Projects/MEIGA/Analisis/annot_pipeline/MEIGA_3.0.1/templated/alignment2consensus.paf"

## Add alignments to the dictionary
PAF = formats.PAF()
PAF.read(PAF_consensus)

for hit in PAF.alignments:
    # Filter out hit if MAPQ == 0
    if hit.MAPQ == 0:
        continue

    # Add hit 
    annotDict[hit.qName]['hits_consensus'].alignments.append(hit)

#### 3.3 Align against personalized transduced regions database (5' transductions)
SAM_transduced = alignment_bwa(FASTA_insert, tdDb, 'alignment2transduced', 2, outDir)     
PAF_transduced = alignment.sam2paf(SAM_transduced, 'alignment2transduced', outDir)
#PAF_transduced = "/Users/brodriguez/Research/Projects/MEIGA/Analisis/annot_pipeline/MEIGA_3.0.1/templated/alignment2transduced.paf"

## Add alignments to the dictionary
PAF = formats.PAF()
PAF.read(PAF_transduced)

for hit in PAF.alignments:
    # Filter out hit if MAPQ == 0
    if hit.MAPQ == 0:
        continue

    # Add hit    
    annotDict[hit.qName]['hits_td_db'].alignments.append(hit)

#### 3.4 Align against reference genome (templated insertions) ####
SAM_ref = alignment_bwa(FASTA_insert, reference, 'alignment2ref', 2, outDir)     
PAF_ref = alignment.sam2paf(SAM_ref, 'alignment2ref', outDir)
#PAF_ref = "/Users/brodriguez/Research/Projects/MEIGA/Analisis/annot_pipeline/MEIGA_3.0.1/templated/alignment2ref.paf"

## Add alignments to the reference
PAF = formats.PAF()
PAF.read(PAF_ref)

for hit in PAF.alignments:
    # Filter out hit if MAPQ == 0
    if hit.MAPQ == 0:
        continue

    # Add hit    
    annotDict[hit.qName]['hits_ref'].alignments.append(hit)

#### 3.5 Align against small RNA database (L1 small-RNA chimera) ####
SAM_snRNA = alignment_bwa(FASTA_insert, smallRNAs, 'alignment2srna', 2, outDir)     
PAF_snRNA = alignment.sam2paf(SAM_snRNA, 'alignment2srna', outDir)
#PAF_snRNA = '/Users/brodriguez/Research/Projects/MEIGA/Analisis/annot_pipeline/MEIGA_3.0.1/templated/alignment2srna.paf'

PAF = formats.PAF()
PAF.read(PAF_snRNA)

for hit in PAF.alignments:
    # Filter out hit if MAPQ == 0
    if hit.MAPQ == 0:
        continue

    # Add hit    
    annotDict[hit.qName]['hits_snRNA'].alignments.append(hit)

## 4. 
###########################################################################################################
for insId in annotDict.keys():


    ## Set up status as unknown
    annotDict[insId]['status'] = 'UNK'

    ### 1. Organize hits into a bin database ###
    ## L1 consensus 
    hitsRefBinDb = hits2binDb(annotDict[insId]['hits_consensus'].alignments)

    ## Transduced region
    hitsTdBinDb = hits2binDb(annotDict[insId]['hits_td_db'].alignments)

    ### 2. Filter hits redundant to L1 consensus ###
    ### Reference  
    maxOverlap = 25
    annotDict[insId]['hits_ref'].alignments = filter_hits(annotDict[insId]['hits_ref'].alignments, hitsRefBinDb, maxOverlap)
    annotDict[insId]['hits_ref'].alignments = filter_hits(annotDict[insId]['hits_ref'].alignments, hitsTdBinDb, maxOverlap)

    ### Transduced regions database
    maxOverlap = 25
    annotDict[insId]['hits_td_db'].alignments = filter_hits(annotDict[insId]['hits_td_db'].alignments, hitsRefBinDb, maxOverlap)

    ### Small nuclear rna database
    maxOverlap = 25
    annotDict[insId]['hits_snRNA'].alignments = filter_hits(annotDict[insId]['hits_snRNA'].alignments, hitsRefBinDb, maxOverlap)
    
    ### 3. Merge PAFs, filter and create alignments chain ###
    PAF_all = formats.PAF()
    PAF_all.alignments = annotDict[insId]['hits_consensus'].alignments + annotDict[insId]['hits_ref'].alignments + annotDict[insId]['hits_td_db'].alignments + annotDict[insId]['hits_snRNA'].alignments

    # Not further processing if no hit
    if not PAF_all.alignments:

        ## a) Solo insertion with unresolved structure
        if annotDict[insId]['polyA'] is None:
            iType = 'solo'

        ## b) Potential Poly(A/T) tract with few no monomeric nucleotides
        else:
            percPolyA = len(annotDict[insId]['polyA'])/annotDict[insId]['iLen']*100
            minPerc = 90

            # PolyA
            if (percPolyA >= minPerc):
                #print('POLY')
                iType = 'poly(A/T)'

            # Unresolved solo
            else:
                #print('UNRESOLVED_SOLO')                
                iType = 'solo'

        # TODO:
        # annotation = [insId, coordId, iType, nbTd, tsdLen, sourceId, tdRegionCoord, tdMAPQ, 'TEMPLATED', '0', len(trimmed), 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', tdSeq]
        #outList.append(annotation)
        continue

    ## Create chains
    maxDist = 10
    maxPercOverlap = 20 
    chain = PAF_all.chain(maxDist, maxPercOverlap)

    if not annotDict[insId]['hits_consensus'].alignments:
        chain_consensus = formats.PAF_chain([])
    else:
        chain_consensus = annotDict[insId]['hits_consensus'].chain(maxDist, maxPercOverlap)

    #print('CHAIN: ', len(chain.alignments), chain.nb_templates()[0], chain.perc_query_covered(), annotDict[insId]['trimmed'])

    templates = [hit.tName for hit in chain.alignments]

    if ("chr" in str(templates)) and (chain.perc_query_covered() >= 90):
        
        chrIns, tmp = annotDict[insId]['coordId'].split(":")
        posIns = int(tmp.split("_")[0])
        
        if ("td" not in templates[0]) and ("chr" in templates[0]):
            annotDict[insId]['templated'] = '5prime'
            annotDict[insId]['templated_coord'] = chain.alignments[0].tName + ':' + str(chain.alignments[0].tBeg) + '-' + str(chain.alignments[0].tEnd)
            annotDict[insId]['templated_len'] = chain.alignments[0].alignmentLen()
            annotDict[insId]['templated_dist'] = chain.alignments[0].tBeg - posIns

        elif ("td" not in templates[-1]) and ("chr" in templates[-1]):
            annotDict[insId]['templated'] = '3prime'
            annotDict[insId]['templated_coord'] = chain.alignments[-1].tName + ':' + str(chain.alignments[-1].tBeg) + '-' + str(chain.alignments[-1].tEnd)
            annotDict[insId]['templated_len'] = chain.alignments[-1].alignmentLen()
            annotDict[insId]['templated_dist'] = chain.alignments[-1].tBeg - posIns

        else:
            for hit in chain.alignments:
                if "chr" not in hit.tName:
                    continue

                if "td" in hit.tName:
                    continue

                annotDict[insId]['templated'] = 'internal'
                annotDict[insId]['templated_coord'] = hit.tName + ':' + str(hit.tBeg) + '-' + str(hit.tEnd)
                annotDict[insId]['templated_len'] = hit.alignmentLen()
                annotDict[insId]['templated_dist'] =hit.tBeg - posIns

    if ('td' in templates[-1]) and (chain.perc_query_covered() >= 90):
        annotDict[insId]['iType'] = 'partnered'
    
    ### 4. Trim 5' transduced sequence ###    
    #trim_5prime_td(insId, annotDict[insId], chain)

    ### 5. Trim 3' transduced sequence ###    
    #trim_3prime_td(insId, annotDict[insId], chain)

    ### 6. Determine insertion structure ###    
    annotDict[insId] = insertion_structure(insId, annotDict[insId], chain_consensus)

## Write annotations as output file
####################################
    

## Create header
header = ['insId', 'coordId', 'iType', 'status', 'sourceId', 'templated', 'templated_coord', 'templated_dist', 'templated_len', 'seq']

## Output list
outList = []

for insId in annotDict:
    outList.append([insId, annotDict[insId]['coordId'], annotDict[insId]['iType'], annotDict[insId]['status'], annotDict[insId]['sourceId'], annotDict[insId]['templated'], annotDict[insId]['templated_coord'], annotDict[insId]['templated_dist'], annotDict[insId]['templated_len'], annotDict[insId]['seq']])


outFile = outDir + '/' + outName + '.tsv'
pd.DataFrame(outList, columns=header).to_csv(outFile, sep="\t", index=False)
